from django.urls import path
from .views import homeDocs

app_name = 'docs'
urlpatterns = [
    path('', homeDocs, name='home_docs_url'),
]
