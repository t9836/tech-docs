# Documentation
1. Deployed at: https://kuymakan-docs.herokuapp.com/
## How to Dev
1. This is a Django App.
2. Just change the "kuymakandocs.json"

## References
1. [Redoc Quick Start](https://redocly.com/docs/redoc/quickstart/intro/)
2. [Redoc HTML](https://redocly.com/docs/redoc/quickstart/html/)